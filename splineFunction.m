function [q1,qd1,qdd1, q2, qd2, qdd2, q3, qd3, qdd3] = splineFunction()
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

x1=[0 2 4 6 8];
y1=[0 30*(pi/180) 45*(pi/180) 150*(pi/180) pi];

x2=[0 2 4 6 8];
y2=[0 -10*(pi/180) 130*(pi/180) 10*(pi/180) 0];

x3=[0 2 4 6 8];
y3=[90*(pi/180) 70*(pi/180) -85*(pi/180) 70*(pi/180) -90*(pi/180)];

xq=0:.1:8;
%position, velocity, and acceleration for J1
s1=spline(x1, y1,xq)

qdd1 = zeros(1,length(s1));
for i= 1:length(s1)
    qdd1(i)= (qd1(i+1)-qd1(i))/.1;
end

%position, velocity, and acceleration for J2
s2=spline(x2,y2,xq);
q2=s2;
qd2= zeros(1,length(s2));
for i= 1:length(s2)-1
    qd2(i)= (q2(i+1)-q2(i))/.1;
end
qdd2= zeros(1,length(s2));
for i= 1:length(s1)-1
    qdd2(i)= (qd2(i+1)-qd2(i))/.1;
end

%position, velocity, and acceleration for J3
s3=spline(x3,y3,xq);
q3=s3;


qdd3= zeros(1:length(s3));
for i= 1:length(s1)-1
    qdd3(i)= (qd3(i+1)-qd3(i))/.1;
end
%How to wait? If it's necessary (make the program wait the time step before
%potting the next point
pause(.1);
end

