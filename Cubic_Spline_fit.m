close all
x1=[0 2 4 6 8];
y1=[0 30*(pi/180) 45*(pi/180) 150*(pi/180) pi];

x2=[0 2 4 6 8];
y2=[0 -10*(pi/180) 130*(pi/180) 10*(pi/180) 0];

x3=[0 2 4 6 8];
y3=[90*(pi/180) 70*(pi/180) -85*(pi/180) 70*(pi/180) -90*(pi/180)];

xq=0:.1:8;

s1=spline(x1, y1,xq);
s2=spline(x2,y2,xq);
s3=spline(x3,y3,xq);

plot(x1,y1,'o',xq,s1)
title('Joints 1, 2, and 3')
xlabel('Time [s]')
ylabel('q [rad]')


hold on

plot(x2,y2,'o',xq,s2)


plot(x3,y3,'o',xq,s3)

legend('J1 given','J1 spline','J2 given', 'J2 spline', 'J3 given', 'J3 spline')
hold off