%%
% 
%   for x = 1:10
%       disp(x)
%   end
% 
function plotRRR(thetas)
%Function Called from simulink model
q1 = thetas(1);
q2 = thetas(2);
q3 = thetas(3);
p_org = [0;0;0];
p_orgE = [0;-1;0];

L1 = 4;
L2 = 3;
L3 = 2;

p1 = [L1;0;0];
p2 = [L2;0;0];
p3 = [L3;0;0];
pE = [0;1;0];

p1_0 = TRANS0_1(q1,p1);
p_org_10 = TRANS0_1(q1,p_org);

p2_0 = TRANS0_2(q1,q2,p2);
p_org_2 = TRANS0_2(q1,q2,p_org);

p3_0 = TRANS0_3(q1,q2,q3,p3);
p_org_3 = TRANS0_3(q1,q2,q3,p_org);

pE_0 = TRANS0_E(q1,q2,q3,pE);
p_org_E = TRANS0_E(q1,q2,q3,p_orgE);



plot([p_org_10(1);p1_0(1)],[p_org_10(2);p1_0(2)])
hold on
plot([p_org_2(1);p2_0(1)],[p_org_2(2);p2_0(2)])
plot([p_org_3(1);p3_0(1)],[p_org_3(2);p3_0(2)])
plot([p_org_E(1);pE_0(1)],[p_org_E(2);pE_0(2)])
grid
axis([-10 10 -10 10]);
hold off

end

