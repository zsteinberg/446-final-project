%q1 = 10*pi/180;q2 = 20*pi/180;q3 = 30*pi/180;
syms q1 q2 q3 q1d q2d q3d q1dd q2dd q3dd grav
m = [ 80*cos(q2 + q3) + 420*cos(q2) + 60*cos(q3) + 12291/20 40*cos(q2 + q3) + 210*cos(q2) + 60*cos(q3) + 2681/20, 40*cos(q2 + q3) + 30*cos(q3) + 101/10;40*cos(q2 + q3) + 210*cos(q2) + 60*cos(q3) + 2681/20 60*cos(q3) + 2681/20 30*cos(q3) + 101/10; 40*cos(q2 + q3) + 30*cos(q3) + 101/10 30*cos(q3) + 101/10 101/10];

c = [0, - 40*sin(q2 + q3) - 210*sin(q2), -40*sin(q2 + q3) - 30*sin(q3); 40*sin(q2 + q3) + 210*sin(q2), 0, -30*sin(q3);40*sin(q2 + q3) + 30*sin(q3), 30*sin(q3), 0];

b = [ - 80*sin(q2 + q3) - 420*sin(q2), - 80*sin(q2 + q3) - 60*sin(q3), - 80*sin(q2 + q3) - 60*sin(q3);                               0,                    -60*sin(q3),                    -60*sin(q3);                      60*sin(q3),                              0,                              0];

grav = 9.81;
g = [15*grav*((3*cos(q1 + q2))/2 + 4*cos(q1)) + 10*grav*(cos(q1 + q2 + q3) + 3*cos(q1 + q2) + 4*cos(q1)) + 40*grav*cos(q1);10*grav*(cos(q1 + q2 + q3) + 3*cos(q1 + q2)) + (45*grav*cos(q1 + q2))/2;10*grav*cos(q1 + q2 + q3)];

f =  [200*q1d;150*q2d;100*q3d];

qd = [q1d;q2d;q3d];

ck = c*qd.^2;
bk = b*[q1d*q2d;q1d*q3d;q2d*q3d];

qdd = [q1dd;q2dd;q3dd];

alpha = m;

beta = ck + bk + f + g;

beta_qd = ck + bk + f;

tau = [1874.034641,558.0821531,84.95709211];

