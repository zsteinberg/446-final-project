p_org = [0;0;0];

L1 = 4;
L2 = 3;
L3 = 2;

p1 = [4;0;0];
p2 = [3;0;0];
p3 = [2;0;0];

p1_0 = TRANS0_1(10*pi/180,p1);
p_org_10 = TRANS0_1(10*pi/180,p_org);

p2_0 = TRANS0_2(10*pi/180,20*pi/180,p2);
p_org_2 = TRANS0_2(10*pi/180,20*pi/180,p_org);

p3_0 = TRANS0_3(10*pi/180,20*pi/180,30*pi/180,p3);
p_org_3 = TRANS0_3(10*pi/180,20*pi/180,30*pi/180,p_org);

hold on
plot([p_org_10(1);p1_0(1)],[p_org_10(2);p1_0(2)])
plot([p_org_2(1);p2_0(1)],[p_org_2(2);p2_0(2)])
plot([p_org_3(1);p3_0(1)],[p_org_3(2);p3_0(2)])
grid
axis square