syms q1 q2 q3 q1d q2d q3d q1dd q2dd q3dd

q1d = 0;
q1dd = 0;
q1 = pi/18;



T1 = (- 40*sin(q2 + q3) - 210*sin(q2))*q2d^2 + (- 80*sin(q2 + q3) - 60*sin(q3))*q2d*q3d - q1d*(80*sin(q2 + q3) + 420*sin(q2))*q2d + (- 40*sin(q2 + q3) - 30*sin(q3))*q3d^2 - q1d*(80*sin(q2 + q3) + 60*sin(q3))*q3d + 15*grav*((3*cos(q1 + q2))/2 + 4*cos(q1)) + q3dd*(40*cos(q2 + q3) + 30*cos(q3) + 101/10) + 10*grav*(cos(q1 + q2 + q3) + 3*cos(q1 + q2) + 4*cos(q1)) + 40*grav*cos(q1) + q2dd*(40*cos(q2 + q3) + 210*cos(q2) + 60*cos(q3) + 2681/20) + q1dd*(80*cos(q2 + q3) + 420*cos(q2) + 60*cos(q3) + 12291/20)
T2 = 150*q2d - 30*q3d^2*sin(q3) + 10*grav*(cos(q1 + q2 + q3) + 3*cos(q1 + q2)) + q3dd*(30*cos(q3) + 101/10) + q2dd*(60*cos(q3) + 2681/20) + (45*grav*cos(q1 + q2))/2 + q1dd*(40*cos(q2 + q3) + 210*cos(q2) + 60*cos(q3) + 2681/20) - 60*q1d*q3d*sin(q3) - 60*q2d*q3d*sin(q3) 
T3 = (40*sin(q2 + q3) + 30*sin(q3))*q1d^2 + 60*sin(q3)*q1d*q2d + 30*sin(q3)*q2d^2 + 100*q3d + (101*q3dd)/10 + q2dd*(30*cos(q3) + 101/10) + q1dd*(40*cos(q2 + q3) + 30*cos(q3) + 101/10) + 10*grav*cos(q1 + q2 + q3)

G1 = 15*grav*((3*cos(q1 + q2))/2 + 4*cos(q1)) + 10*grav*(cos(q1 + q2 + q3) + 3*cos(q1 + q2) + 4*cos(q1)) + 40*grav*cos(q1)

Tor1 = (- 40*sin(q2 + q3) - 210*sin(q2))*q2d^2 + (- 80*sin(q2 + q3) - 60*sin(q3))*q2d*q3d - q1d*(80*sin(q2 + q3) + 420*sin(q2))*q2d + (- 40*sin(q2 + q3) - 30*sin(q3))*q3d^2 - q1d*(80*sin(q2 + q3) + 60*sin(q3))*q3d + 15*grav*((3*cos(q1 + q2))/2 + 4*cos(q1)) + q3dd*(40*cos(q2 + q3) + 30*cos(q3) + 101/10) + 10*grav*(cos(q1 + q2 + q3) + 3*cos(q1 + q2) + 4*cos(q1)) + 40*grav*cos(q1) + q2dd*(40*cos(q2 + q3) + 210*cos(q2) + 60*cos(q3) + 2681/20) + q1dd*(80*cos(q2 + q3) + 420*cos(q2) + 60*cos(q3) + 12291/20)

