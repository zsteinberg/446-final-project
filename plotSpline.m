function time = plotSpline(q)
%plotSpline Summary of this function goes here
%   plot of the Spline positions
q1 = q(1);
q2 = q(2);
q3 = q(3);
plot(time, q1,'blue',time,q2,'red',time,q3,'green')
time = time +0.1;
end

