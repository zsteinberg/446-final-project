function newPoint6 = Trans3_E(theta,p)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
syms L3
%L3=2;

point = [p(1);p(2);p(3);1];
T = [1 0 0 L3; 0 1 0 0; 0 0 1 0; 0 0 0 1];
newPoint6 = T*point;
end