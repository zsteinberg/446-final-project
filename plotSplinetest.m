figure
plot(q)
title('Spline Position Curve Input');
legend('Joint 1','Joint 2','Joint 3');
xlim([0 0.55]);
xticks(0:0.063125:0.505);
xticklabels({'0','1','2','3','4','5','6','7','8'});
xlabel('Time (s)');
ylabel('q [rad]');

% figure
% plot(qd)
% title('Spline Velocity Curve Input');
% xlim([0 0.505]);
% xticks(0:0.063125:0.505);
% xticklabels({'0','1','2','3','4','5','6','7','8'});
% legend('Joint 1','Joint 2','Joint 3');
% xlabel('Time (s)');
% ylabel('qd [velocity]');