M = [ 80*cos(q2 + q3) + 420*cos(q2) + 60*cos(q3) + 12291/20
      40*cos(q2 + q3) + 210*cos(q2) + 60*cos(q3) + 2681/20 
      40*cos(q2 + q3) + 30*cos(q3) + 101/10;

      40*cos(q2 + q3) + 210*cos(q2) + 60*cos(q3) + 2681/20 
      60*cos(q3) + 2681/20
      30*cos(q3) + 101/10;
      
      40*cos(q2 + q3) + 30*cos(q3) + 101/10
      30*cos(q3) + 101/10 
      101/10];


G 
15*g*((3*cos(q1 + q2))/2 + 4*cos(q1)) + 10*g*(cos(q1 + q2 + q3) + 3*cos(q1 + q2) + 4*cos(q1)) + 40*g*cos(q1); 
10*g*(cos(q1 + q2 + q3) + 3*cos(q1 + q2)) + (45*g*cos(q1 + q2))/2;
10*g*cos(q1 + q2 + q3)];

C_q
[                            0, - 40*sin(q2 + q3) - 210*sin(q2), - 40*sin(q2 + q3) - 30*sin(q3)]
[    40*sin(q2+q3)+210*sin(q2),                               0,                    -30*sin(q3)]
[ 40*sin(q2 + q3) + 30*sin(q3),                      30*sin(q3),                              0]


B_q
[ - 80*sin(q2 + q3) - 420*sin(q2), - 80*sin(q2 + q3) - 60*sin(q3), - 80*sin(q2 + q3) - 60*sin(q3)]
[                               0,                    -60*sin(q3),                    -60*sin(q3)]
[                      60*sin(q3),                              0,                              0]