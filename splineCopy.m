%%
% 
%   for x = 1:10
%       disp(x)
%   end
% 
function [q,qd,count] = fcn(count)

x1=[0 2 4 6 8];
y1=[0 30*(pi/180) 45*(pi/180) 150*(pi/180) pi];

x2=[0 2 4 6 8];
y2=[0 -10*(pi/180) 130*(pi/180) 10*(pi/180) 0];

x3=[0 2 4 6 8];
y3=[90*(pi/180) 70*(pi/180) -85*(pi/180) 70*(pi/180) -90*(pi/180)];

xq=0:.1:8;

s1 = spline(x1, y1,xq);
s2 = spline(x2,y2,xq);
s3 = spline(x3,y3,xq);

%Velocity stuff
q1=s1;
q2=s2;
q3=s3;

if count == 81
    qd1= 0;
    qd2= 0;
    qd3= 0;
else
    qd1= (q1(count+1)-q1(count))/.1;
    qd2= (q2(count+1)-q2(count))/.1;
    qd3= (q3(count+1)-q3(count))/.1;
end

q = [s1(count);s2(count);s3(count)];
qd = [qd1; qd2; qd3];



if count < 81
count = count+1;
end