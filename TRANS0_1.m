function newPoint = TRANS0_1(theta,p)
%Transformation Matrix from Frame 1 to Frame 0
point = [p(1);p(2);p(3);1];
T = [cos(theta) -sin(theta) 0 0; sin(theta) cos(theta) 0 0; 0 0 1 0; 0 0 0 1];
newPoint = T*point;
end

