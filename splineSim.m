x1=[0 2 4 6 8];
y1=[0 30*(pi/180) 45*(pi/180) 150*(pi/180) pi];

x2=[0 2 4 6 8];
y2=[0 -10*(pi/180) 130*(pi/180) 10*(pi/180) 0];

x3=[0 2 4 6 8];
y3=[90*(pi/180) 70*(pi/180) -85*(pi/180) 70*(pi/180) -90*(pi/180)];

xq=0:.1:8;
i = 0;
count = 1;
time = 0;
while i < 8
   t(count) = i;
   i = i + 0.1;
   count = count + 1;
end
s1 = spline(x1, y1,xq);
s2 = spline(x2,y2,xq);
s3 = spline(x3,y3,xq);

plot(t, s1, 'blue', t, s2, 'red', t, s3, 'green')
% i = 1;
% while i < 82
%     thetas = [s1(i);s2(i);s3(i)];
%     plotRRR(thetas);
%     pause(0.1);
%     i = i+1;
% end
