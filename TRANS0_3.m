function newPoint = TRANS0_3(theta1,theta2,theta3,p)
%%Transformation Matrix from Frame 3 to Frame 0

point = [p(1);p(2);p(3);1];
T = [cos(theta1+theta2+theta3) -sin(theta1+theta2+theta3) 0 3*cos(theta1+theta2)+4*cos(theta1); sin(theta1+theta2+theta3) cos(theta1+theta2+theta3) 0 3*sin(theta1+theta2)+4*sin(theta1); 0 0 1 0; 0 0 0 1];
newPoint = T*point;
end

