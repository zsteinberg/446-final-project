function newPoint = TRANS0_2(theta1,theta2, p)
%%Transformation Matrix from Frame 2 to Frame 0
point = [p(1);p(2);p(3);1];
T = [cos(theta1+theta2) -sin(theta1+theta2) 0 4*cos(theta1); sin(theta1+theta2) cos(theta1+theta2) 0 4*sin(theta1); 0 0 1 0; 0 0 0 1];
newPoint = T*point;
end

