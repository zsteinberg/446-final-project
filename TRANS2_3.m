function newPoint5 = TRANS2_3(theta,p)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
point = [p(1);p(2);p(3);1];
T = [cos((theta)) -sin((theta)) 0 0; sin((theta)) cos((theta)) 0 0; 0 0 1 0; 0 0 0 1];
newPoint5 = T*point;
end