%%
% 
%   for x = 1:10
%       disp(x)
%   end
% 
syms mMatrix vMatrix gMatrix F
syms Pc1 Pc2 Pc3 q1d q2d q3d
syms q1dd q2dd q3dd q1 q2 q3
syms T0_1 T1_2 T2_3 T0_2 T0_3 T3_E T0_E
syms L1 L2 L3 mass1 mass2 mass3 grav
syms Ic Ixx1 Iyy1 Izz1 Ixx2 Iyy2 Izz2 Ixx3 Iyy3 Izz3

% q's are thetas
% qdd's are double derivatives

mass1 = 20;
mass2 = 15;
mass3 = 10;

Izz1 = 0.5;
Izz2 = 0.2;
Izz3 = 0.1;

L1 = 4;
L2 = 3;
L3 = 2;

%q1 = 10*pi/180;q2 = 20*pi/180;q3 = 30*pi/180;

q = [q1;q2;q3];
qT = q.';
qd = [q1d;q2d;q3d];
qdT = qd.';

qdd = [q1dd;q2dd;q3dd];
qddT = qdd.';

%Center of Mass Points
Pc1 = [L1/2; 0; 0];
Pc2 =  [L2/2; 0; 0];
Pc3 = [L3/2; 0; 0];

%Transformation Matrices
T0_1 = [cos(q1) -sin(q1) 0 0; sin(q1) cos(q1) 0 0; 0 0 1 0; 0 0 0 1];
T0_2 = [cos(q1+q2) -sin(q1+q2) 0 L1*cos(q1); sin(q1+q2) cos(q1+q2) 0 L1*sin(q1); 0 0 1 0; 0 0 0 1];
T0_3 = [cos(q1+q2+q3) -sin(q1+q2+q3) 0 L2*cos(q1+q2)+L1*cos(q1); sin(q1+q2+q3) cos(q1+q2+q3) 0 L2*sin(q1+q2)+L1*sin(q1); 0 0 1 0; 0 0 0 1];
T0_E = [cos(q1+q2+q3) -sin(q1+q2+q3) 0 L3*cos(q1+q2+q3)+L2*cos(q1+q2)+L1*cos(q1); sin(q1+q2+q3) cos(q1+q2+q3) 0 L3*sin(q1+q2+q3)+L2*sin(q1+q2)+L1*sin(q1); 0 0 1 0; 0 0 0 1];

%Transformed center of mass points
TPc1 = T0_1*[Pc1(1);Pc1(2);Pc1(3); 1];
TPc2 = T0_2*[Pc2(1);Pc2(2);Pc2(3); 1];
TPc3 = T0_3*[Pc3(1);Pc3(2);Pc3(3); 1];

%Jvi
Jac1 = jacobian(TPc1(1:3), qT);
Jac2 = jacobian(TPc2(1:3), qT);
Jac3 = jacobian(TPc3(1:3),qT);

%Transpose Jvi
Jv1T = Jac1.';
Jv2T = Jac2.';
Jv3T = Jac3.';

%Jwi
Jw1 = [0 0 0; 0 0 0; 1 0 0];
Jw2 = [0 0 0; 0 0 0; 1 1 0];
Jw3 = [0 0 0; 0 0 0; 1 1 1];

%Jwi Transpose
Jw1T = Jw1.';
Jw2T = Jw2.';
Jw3T = Jw3.';

%Vci 
Vc1 = Jac1.*qd;
Vc2 = Jac2.*qd;
Vc3 = Jac3.*qd;

%wi
w1 = Jw1.*qd;
w2 = Jw2.*qd;
w3 = Jw3.*qd;

%Transpose wi
w1T = w1.';
w2T = w2.';
w3T = w3.';

%Transpose Vci
TVc1 = Vc1.';
TVc2 = Vc2.';
TVc3 = Vc3.';

%Ici
Ic1 = diag([Ixx1,Iyy1,Izz1]);
Ic2 = diag([Ixx2,Iyy2,Izz2]);
Ic3 = diag([Ixx3,Iyy3,Izz3]);

%Mvi
Mv1 = simplify((Jac1.'*Jac1).*mass1);
Mv2 = simplify((Jac2.')*(Jac2).*mass2);
Mv3 = simplify((Jac3.')*(Jac3).*mass3);

%Mwi
Mw1 = (Jw1T*Ic1*Jw1);
Mw2 = (Jw2T*Ic2*Jw2);
Mw3 = (Jw3T*Ic3*Jw3);


M1 = Mv1 + Mw1;
M2 = Mv2 + Mw2;
M3 = Mv3 + Mw3;

%Mass Matrix
Mass = M1 + M2 + M3;

%Mass 
m11 = Mass(1,1);
m12 = Mass(1,2);
m13 = Mass(1,3);
m21 = Mass(2,1);
m22 = Mass(2,2); 
m23 = Mass(2,3);
m31 = Mass(3,1);
m32 = Mass(3,2);
m33 = Mass(3,3);

%Forces for G matrix
%Gravity is a vector
gVec = [0; -grav; 0];
massGrav1 = mass1.*gVec;
massGrav2 = mass2.*gVec;
massGrav3 = mass3.*gVec;

massGrav = [massGrav1, massGrav2, massGrav3];

JacG1 = Jv1T*massGrav1;
JacG2 = Jv2T*massGrav2;
JacG3 = Jv3T*massGrav3;
%Jacs = -1.*([Jv1T, Jv2T, Jv3T]*massGrav);
JacG = -1.*(JacG1 + JacG2 + JacG3);
simplify(JacG);

%gMatrix = (Jv1T*massGrav + Jv2T*massGrav + Jv3T*massGrav);

%Components for coriolis
%Partial derivatives
m111=diff(m11,q1);
m112=diff(m11,q2);
m113=diff(m11,q3);
m121=diff(m12,q1);
m122=diff(m12,q2);
m123=diff(m12,q3);
m131=diff(m13,q1);
m132=diff(m13,q2);
m133=diff(m13,q3);
m211=diff(m21,q1);
m212=diff(m21,q2);
m213=diff(m21,q3);
m221=diff(m22,q1);
m222=diff(m22,q2);
m223=diff(m22,q3);
m231=diff(m23,q1);
m232=diff(m23,q2);
m233=diff(m23,q3);
m311=diff(m31,q1);
m312=diff(m31,q2);
m313=diff(m31,q3);
m321=diff(m32,q1);
m322=diff(m32,q2);
m323=diff(m32,q3);
m331=diff(m33,q1);
m332=diff(m33,q2);
m333=diff(m33,q3);

%Christoffel Symbols
b111 = .5.*(m111+m111-m111);
b112 = .5.*(m112+m121-m121);
b113 = .5.*(m113+m131-m131);
b121 = .5.*(m121+m112-m211);
b122 = .5.*(m122+m122-m221);
b123 = .5.*(m123+m132-m231);
b131 = .5.*(m131+m113-m311);
b132 = .5.*(m132+m123-m321);
b133 = .5.*(m133+m133-m331);
b211 = .5.*(m211+m211-m112);
b212 = .5.*(m212+m221-m122);
b213 = .5.*(m213+m231-m132);
b221 = .5.*(m221+m212-m212);
b222 = .5.*(m222+m222-m222);
b223 = .5.*(m223+m232-m232);
b231 = .5.*(m231+m213-m312);
b232 = .5.*(m232+m223-m322);
b233 = .5.*(m233+m233-m332);
b311 = .5.*(m311+m311-m113);
b312 = .5.*(m312+m321-m123);
b313 = .5.*(m313+m331-m133);
b321 = .5.*(m321+m312-m231);
b322 = .5.*(m322+m322-m223);
b323 = .5.*(m323+m332-m233);
b331 = .5.*(m331+m313-m313);
b332 = .5.*(m332+m323-m323);
b333 = .5.*(m333+m333-m333);

%Centrifugal Matrix
C_q = [b111, b122, b133; b211, b222, b233; b311, b322, b333];
C = simplify(C_q);

C_qs =[(q1d).^2; (q2d).^2; (q3d).^2];
%Coriolis Matrix    
B_q = [2.*b112, 2.*b113, 2.*b123; 2.*b212, 2.*b213, 2.*b223; 2.*b312, 2.*b313, 2.*b323];
B = simplify(B_q);
B_qs =[q1d.*q2d; q1d.*q3d; q2d.*q3d];


%Complete velocity matrix
V = C_q*C_qs + B_q*B_qs;

simplify(V);

Torque = Mass*qdd + V + JacG;
qdd = Mass^(-1)*(Torque - V - JacG);



%Friction
SFric1 = 0;
DFric1 = 200;

J1Fric = SFric1*sign(q1d) + DFric1*q1d;

SFric2 = 0;
DFric2 = 150;
J2Fric = SFric2*sign(q2d) + DFric2*q2d;

SFric3 = 0;
DFric3 = 100;
J3Fric = SFric3*sign(q3d) + DFric3*q3d;

Fr = [J1Fric; J2Fric; J3Fric];

%Dynamic Equation
Torque = Mass*qdd + V + JacG + Fr;

%Solved for Acceleration
qdd = Mass^(-1)*(Torque - V - JacG - Fr);
