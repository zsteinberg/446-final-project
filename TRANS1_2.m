function newPoint4 = TRANS1_2(theta,p)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
syms L1
%L1 = 4;
point = [p(1);p(2);p(3);1];
T = [cos((theta)) -sin((theta)) 0 L1; sin((theta)) cos((theta)) 0 0; 0 0 1 0; 0 0 0 1];
newPoint4 = T*point;
end